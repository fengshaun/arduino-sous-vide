#include <stdlib.h>
#include <LiquidCrystal_I2C.h>

LiquidCrystal_I2C lcd(0x27, 20, 4);

void _set_temp(const char *prompt,
              double temp,
              int cursor_x,
              int cursor_y) {
    lcd.setCursor(cursor_x, cursor_y);
    lcd.print(prompt);

    char num[5] = {0, 0, 0, 0, 0};
    // arduino's sprintf does not include %f
    dtostrf(temp, 4, 1, num);
    lcd.print(num);

    // trailing spaces to clear the rest of the line
    lcd.print("C  ");
}


void display_init() {
    lcd.init();
    lcd.backlight();
}

void display_set_target_temp(double target) {
    _set_temp(" Target: ", target, 0, 0);
}

void display_set_current_temp(double cur) {
    _set_temp("Current: ", cur, 0, 1);
}

void display_reset_current_temp() {
    lcd.setCursor(0, 1);
    lcd.print("Current: UNKNOWN");
}

void display_reset_target_temp() {
    lcd.setCursor(0, 0);
    lcd.print(" Target: NOT SET");
}
