void test_display() {
  double temps[4] = {5, 6.5, 23, 23.2};
  for (int i = 0; i < 4; i++) {
    display_set_current_temp(temps[i]);
    display_set_target_temp(temps[i]);
    delay(1000);
  }
}

void run_tests() {
  test_display();
}
