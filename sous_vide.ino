#include <math.h>
#include <OneWire.h>
#include <DallasTemperature.h>

#include "display.h"

// include last
#include "tests.h"

#define UP_TEMP_PIN 3
#define DOWN_TEMP_PIN 2
#define TEMP_SENSOR_PIN 8
#define HEATER_CONTROL_PIN 9

int relay_is_on = 0;

volatile double target_temp = 30;
volatile bool update_target_temp = false;

double current_temp = 0;

long unsigned current_millis = 0;
long unsigned prev_millis = 0;
bool temperature_requested = false;

OneWire temp_sensor_bus(TEMP_SENSOR_PIN);
DallasTemperature sensors(&temp_sensor_bus);

void increase_target_temp() {
  target_temp = min(target_temp + 0.5, 90);
  update_target_temp = true;
}

void decrease_target_temp() {
  target_temp = max(target_temp - 0.5, 30);
  update_target_temp = true;
}

void setup() {
  Serial.begin(9600);
  sensors.begin();
  sensors.setWaitForConversion(false);

  pinMode(UP_TEMP_PIN, INPUT);
  pinMode(DOWN_TEMP_PIN, INPUT);

  pinMode(HEATER_CONTROL_PIN, OUTPUT);
  digitalWrite(HEATER_CONTROL_PIN, HIGH);

  attachInterrupt(digitalPinToInterrupt(UP_TEMP_PIN), increase_target_temp, FALLING);
  attachInterrupt(digitalPinToInterrupt(DOWN_TEMP_PIN), decrease_target_temp, FALLING);
  
  display_init();
  display_set_target_temp(target_temp);

  current_millis = 0;
  prev_millis = millis();
  temperature_requested = false;
}

void turn_on_heater() {
  // The relay is triggered when the input goes below about 2 V
  digitalWrite(HEATER_CONTROL_PIN, LOW);
}

void turn_off_heater() {
  digitalWrite(HEATER_CONTROL_PIN, HIGH);
}

void loop() {
  current_millis = millis();

  if (update_target_temp) {
    display_set_target_temp(target_temp);
    update_target_temp = false;
  }

  // request temperature reading every second
  // this looks a lot like a state machine!
  if (!temperature_requested
      && (current_millis - prev_millis) > 1000)
    {
      sensors.requestTemperatures();
      temperature_requested = true;
      Serial.println("temperature requested millis=" + String(millis()));
      prev_millis = current_millis;

    } else if (temperature_requested
               && (current_millis - prev_millis) > ((unsigned int16_t) sensors.millisToWaitForConversion(9))
               && sensors.isConversionComplete())
    {
      current_temp = sensors.getTempCByIndex(0);
      Serial.println("temperature = " + String(current_temp) + " millis=" + String(millis()));
      display_set_current_temp(current_temp);

      // only need to update heater status when new temperature reading is done
      if ((current_temp < target_temp) && (abs(current_temp - target_temp) > 0.1)) {
        turn_on_heater();
      } else {
        turn_off_heater();
      }

      // try to get new temperature
      temperature_requested = false;
      prev_millis = current_millis;
    }
}
